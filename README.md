# EduClasses #

The Educlasses Repository

### Dependencies ###

* npm install
* npm install express --save
* npm install express-ejs-layouts --save
* npm install mysql --save
* npm install aws-sdk --save
* npm install formidable --save

## To run it forever ##

* cd /path/to/eduClasses
* chmod u+x eduClassesServer.sh
* sudo ./eduClassesServer.sh


## To kill the process ##

* sudo pkill -f "node eduClassesServer.js"
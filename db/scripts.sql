show databases;
create database eduDB;
create user 'eduUser' identified by '***********'; 
grant all privileges on eduDB.* to eduUser;
create user 'userImporter' identified by '***********'; 
grant select, show view on eduDB.* to userImporter;
grant insert, update, delete on eduDB.il_user_import to userImporter;
grant insert, update, delete on eduDB.il_node_contact to userImporter;
create user 'souvik' identified by '***********'; 
grant all privileges on eduDB.* to souvik;
grant insert, update, delete on eduDB.IL_BANK_STATEMENT to userImporter;
GRANT INSERT, UPDATE, DELETE ON eduDB.IL_BANK_STATEMENT_DTL TO userImporter;
grant insert, update, delete on eduDB.il_mob_mail_upd to userImporter;

use eduDB;

create table cl_user_type (
type_id int primary key auto_increment,
type_desc varchar(100) not null
);

create table cl_user_status (
status_id int primary key auto_increment,
status_desc varchar(100) not null
);

create table cl_node_mst (
node_id int primary key auto_increment,
node_name varchar(500) not null,
node_desc varchar(500)
);

alter table cl_node_mst 
add column district varchar(100),
add column inst_type varchar(1),
add column inst_address varchar(1000),
add column ins_medium varchar(2),
add column intake int,
add constraint cl_node_mst_c1 check (inst_type in ('P','G')),
add constraint cl_node_mst_c2 check (ins_medium in ('B','E','H', 'N','U'));

alter table cl_node_mst 
add column mst_srl int,
add column dist_srl int,
add column col_code varchar(10);

alter table cl_node_mst
add column payment_confirmed timestamp;

create unique index cl_node_mst_n1 on cl_node_mst(col_code);

create table cl_user_mst (
user_id int primary key auto_increment,
user_name varchar(500) not null,
user_type int not null,
user_node int not null,
user_secret varchar(64) not null,
user_status int not null,
foreign key (user_type) references cl_user_type (type_id),
foreign key (user_status) references cl_user_status (status_id),
foreign key (user_node) references cl_node_mst (node_id)
);

alter table cl_user_mst 
add column mobile_no varchar(10),
add column email_id varchar(500),
add column ins_medium varchar(2),
add constraint cl_user_mst_c1 check (ins_medium in ('E','B','H'));

ALTER TABLE cl_user_mst
DROP CHECK cl_user_mst_c1;

ALTER TABLE cl_user_mst
add constraint cl_user_mst_c1 check (ins_medium in ('B','E','H', 'N','U'));

alter table cl_user_mst
add column welcome_sms_date varchar(50);

create unique index cl_user_mst_n1 on cl_user_mst (user_secret);
create index cl_user_mst_n2 on cl_user_mst(welcome_sms_date);

create table ui_video_link (
link_id int primary key auto_increment,
link varchar(500) not null,
video_info varchar(2000) not null,
is_active varchar(1) NOT NULL DEFAULT 'Y',
created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
creator_secret varchar(64),
constraint ui_video_link_c1 check (is_active in ('Y', 'N')),
foreign key (creator_secret) references cl_user_mst (user_secret)
);

alter table ui_video_link
add column class_title varchar(1000);

alter table ui_video_link
add column start_time_str varchar(5),
add column end_time_str varchar(5);

alter table ui_video_link
add column video_date TIMESTAMP default current_timestamp;

alter table ui_video_link
add column is_default varchar(1) not null default 'N';

alter table ui_video_link 
add column next_class timestamp;

create table ui_schedule_txt (
schedule_id int primary key auto_increment,
markup varchar(500) not null,
is_active varchar(1) NOT NULL DEFAULT 'Y',
created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
creator_secret varchar(64),
constraint ui_schedule_txt_c1 check (is_active in ('Y', 'N')),
foreign key (creator_secret) references cl_user_mst (user_secret)
);

alter table ui_schedule_txt modify column markup varchar(4000);

create table ui_assignment_txt (
assignment_id int primary key auto_increment,
assignment_count int not null default 1,
assignment_text varchar(1000) not null,
is_active varchar(1) NOT NULL DEFAULT 'Y',
created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
creator_secret varchar(64),
constraint ui_assignment_txt_c1 check (is_active in ('Y', 'N')),
foreign key (creator_secret) references cl_user_mst (user_secret)
);

alter table ui_assignment_txt
add column upload_open_date timestamp not null default current_timestamp,
add column upload_close_date timestamp not null default current_timestamp;

create table il_user_attendance (
attendance_id int primary key auto_increment,
user_id int not null,
login_time timestamp not null default current_timestamp,
foreign key (user_id) references cl_user_mst (user_id)
);

create index il_user_attendance_n1 on il_user_attendance(user_id);

create table il_user_assignment (
assignment_id int primary key auto_increment,
user_id int not null,
original_name varchar(500) not null,
modified_name varchar(500) not null,
upload_time timestamp not null default current_timestamp,
foreign key (user_id) references cl_user_mst (user_id)
);

create index il_user_assignment_n1 on il_user_assignment(user_id);

create table il_user_import (
user_name varchar(1000),
mobile_no varchar(10),
mainl_id varchar(1000),
college_id varchar(100),
student_code varchar(100)
);

create index il_user_import_n1 on il_user_import (college_id);
create unique index il_user_import_n2 on il_user_import (college_id, student_code);

alter table il_user_import
add column import_date timestamp;

create table il_node_contact (
col_code varchar(100),
mobile_no varchar(100),
email_id varchar(1000)
);

create table cl_app_status (
status_code varchar(10) primary key);
insert into cl_app_status (status_code) values ('ACTIVE');

create table IL_BANK_STATEMENT (
  BANK_SRL int(11) DEFAULT NULL,
  BANK_DATE varchar(45) DEFAULT NULL,
  BANK_DESC varchar(1000) DEFAULT NULL,
  BANK_AMOUNT varchar(45) DEFAULT NULL,
  COL_CODE varchar(45) DEFAULT NULL,
  PART_PAYMENT varchar(45) DEFAULT NULL,
  PART_PAYMENT_SRL int(11) DEFAULT NULL,
  USER_IMPORT varchar(2) DEFAULT NULL,
  CONFIRM_BANK_AMOUNT varchar(45) DEFAULT NULL
);

ALTER TABLE IL_BANK_STATEMENT
ADD PRIMARY KEY (BANK_SRL);

CREATE TABLE `IL_BANK_STATEMENT_DTL` (
  `BANK_SRL` int(11) NOT NULL,
  `MULTI_CODE` varchar(10) NOT NULL,
  `COL_CODE` varchar(45) DEFAULT NULL,
  `PART_PAYMENT` varchar(45) DEFAULT NULL,
  `PART_PAYMENT_SRL` int(11) DEFAULT NULL,
  `USER_IMPORT` varchar(2) DEFAULT NULL,
  `CONFIRM_BANK_AMOUNT` varchar(45) DEFAULT NULL,
  `REMARKS` varchar(100) DEFAULT NULL
);

create table il_mob_mail_upd (
user_code varchar(50) not null,
new_mobile varchar(10),
new_mail varchar(500),
update_req_date timestamp not null default current_timestamp,
cl_update_date timestamp
);

create index il_mob_mail_upd_n1 on il_mob_mail_upd (user_code);

alter table il_mob_mail_upd add foreign key (user_code) references cl_user_mst(user_secret);


delimiter // 

create trigger trg_generate_user_secret 
    before insert
    on cl_user_mst for each row 
    begin
        set NEW.user_secret = substring(MD5(RAND()),1,32);
	end//  
    
-- for the error: You do not have the SUPER privilege and binary logging is enabled : add a parameter group and set log_bin_trust_function_creatorsto 1

-- insert into cl_user_mst (user_name, user_type, user_node, user_status) values ('Sibashis', 1, 1, 1);

-- add a parameter group and set time_zone to Asia/Calcutta

drop trigger trg_generate_user_secret;

-- Nodal user creation

insert into cl_user_mst
(user_name, user_type, user_node, user_secret, user_status, mobile_no, email_id, ins_medium)
select 'The Secretary' user_name, 2 user_type, node_id user_node, 
concat(a.col_code, LEFT(md5(rand()), 3)) user_secret, 1 user_status, null mobile_no, null email_id, 'E' ins_medium
from cl_node_mst a;

update cl_user_mst x
set mobile_no = (select mobile_no from il_node_contact y where left(x.user_secret,6) = y.col_code)
where exists (select 1 from il_node_contact y where left(x.user_secret,6) = y.col_code)
and x.mobile_no is null
and x.user_type = 2;

select mobile_no, concat('Dear Secretary, ',b.node_name, ', Yoor access code for D.El.Ed Part II Online Classes is ', user_secret, '. Click here http://edu.educlasses.in/access?usercode=', user_secret) sms
from cl_user_mst a, cl_node_mst b
where a.user_node = b.node_id
and a.mobile_no is not null
and a.user_type = 2;

-- Manually confirmed payment as per discussion with Mr. Souvik Bhattacharya 23.08.20
update cl_node_mst set payment_confirmed = current_timestamp where col_code in ('MU036','MU098','HO015');

-- Manually confirmed payment as per discussion with Mr. Souvik Bhattacharya 27.08.20
update cl_node_mst set payment_confirmed = current_timestamp where col_code in ('NP022','NP017','NP038','NP037','NP006');

-- There was a node admin user created with secret NP022066, clashed with an actual user; updated manually to NP022Yrr
-- also to ensure such clash is avoided:
update cl_user_mst set user_secret = concat(user_secret, 'N') where user_type = 2;

-- Manually updated default video on 04.12.2020 (mr. Souvik Bhattacharya)
-- ID: 20; OLD: https://www.youtube-nocookie.com/embed/Zfb4ZpsW3Fw | GUIDE TO D.El.Ed Part II (REGULAR/FACE TO FACE) EXAMINATION (THEORETICAL) – 2018-2020 | Topic : "GUIDE TO D.El.Ed Part II (REGULAR/FACE TO FACE) EXAMINATION (THEORETICAL) – 2018-2020" By : EduClasses Online 
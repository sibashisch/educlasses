use eduDB;

select * from cl_node_mst;

insert into cl_node_mst
select * from cl_node_mst_1 where node_id = 1;

select * from cl_user_mst;

insert into cl_user_mst
select * from cl_user_mst_1 where user_id <= 20 and user_node = 1;

commit;


alter table cl_node_mst 
drop check cl_node_mst_2_c2;

alter table cl_node_mst
add constraint cl_node_mst_2_c2 check (ins_medium in ('B','E','H', 'N','U','S'));


commit;

-- Import Master Data from .csv

select * from cl_node_mst;
USE eduDB;

RENAME TABLE cl_node_mst TO cl_node_mst_1;
RENAME TABLE cl_session_info TO cl_session_info_1;
RENAME TABLE cl_user_mst TO cl_user_mst_1;
RENAME TABLE IL_BANK_STATEMENT TO IL_BANK_STATEMENT_1;
RENAME TABLE IL_BANK_STATEMENT_DTL TO IL_BANK_STATEMENT_DTL_1;
RENAME TABLE il_node_contact TO il_node_contact_1;
RENAME TABLE il_user_import TO il_user_import_1;

create table cl_node_mst (
node_id int primary key auto_increment,
node_name varchar(500) not null,
node_desc varchar(500)
);

alter table cl_node_mst 
add column district varchar(100),
add column inst_type varchar(1),
add column inst_address varchar(1000),
add column ins_medium varchar(2),
add column intake int,
add constraint cl_node_mst_2_c1 check (inst_type in ('P','G')),
add constraint cl_node_mst_2_c2 check (ins_medium in ('B','E','H', 'N','U'));

alter table cl_node_mst 
add column mst_srl int,
add column dist_srl int,
add column col_code varchar(10);

alter table cl_node_mst
add column payment_confirmed timestamp;

create unique index cl_node_mst_2_n1 on cl_node_mst(col_code);

create table cl_user_mst (
user_id int primary key auto_increment,
user_name varchar(500) not null,
user_type int not null,
user_node int not null,
user_secret varchar(64) not null,
user_status int not null,
foreign key (user_type) references cl_user_type (type_id),
foreign key (user_status) references cl_user_status (status_id),
foreign key (user_node) references cl_node_mst (node_id)
);

alter table cl_user_mst 
add column mobile_no varchar(10),
add column email_id varchar(500),
add column ins_medium varchar(2),
add constraint cl_user_mst_2_c1 check (ins_medium in ('E','B','H'));

ALTER TABLE cl_user_mst
DROP CHECK cl_user_mst_2_c1;

ALTER TABLE cl_user_mst
add constraint cl_user_mst_2_c1 check (ins_medium in ('B','E','H', 'N','U'));

alter table cl_user_mst
add column welcome_sms_date varchar(50);

create unique index cl_user_mst_2_n1 on cl_user_mst (user_secret);
create index cl_user_mst_2_n2 on cl_user_mst(welcome_sms_date);

create table il_user_import (
user_name varchar(1000),
mobile_no varchar(10),
mainl_id varchar(1000),
college_id varchar(100),
student_code varchar(100)
);

create index il_user_import_2_n1 on il_user_import (college_id);
create unique index il_user_2_import_n2 on il_user_import (college_id, student_code);

alter table il_user_import
add column import_date timestamp;

create table il_node_contact (
col_code varchar(100),
mobile_no varchar(100),
email_id varchar(1000)
);

create table IL_BANK_STATEMENT (
  BANK_SRL int(11) DEFAULT NULL,
  BANK_DATE varchar(45) DEFAULT NULL,
  BANK_DESC varchar(1000) DEFAULT NULL,
  BANK_AMOUNT varchar(45) DEFAULT NULL,
  COL_CODE varchar(45) DEFAULT NULL,
  PART_PAYMENT varchar(45) DEFAULT NULL,
  PART_PAYMENT_SRL int(11) DEFAULT NULL,
  USER_IMPORT varchar(2) DEFAULT NULL,
  CONFIRM_BANK_AMOUNT varchar(45) DEFAULT NULL
);

ALTER TABLE IL_BANK_STATEMENT
ADD PRIMARY KEY (BANK_SRL);

CREATE TABLE `IL_BANK_STATEMENT_DTL` (
  `BANK_SRL` int(11) NOT NULL,
  `MULTI_CODE` varchar(10) NOT NULL,
  `COL_CODE` varchar(45) DEFAULT NULL,
  `PART_PAYMENT` varchar(45) DEFAULT NULL,
  `PART_PAYMENT_SRL` int(11) DEFAULT NULL,
  `USER_IMPORT` varchar(2) DEFAULT NULL,
  `CONFIRM_BANK_AMOUNT` varchar(45) DEFAULT NULL,
  `REMARKS` varchar(100) DEFAULT NULL
);

insert into cl_user_mst
(user_name, user_type, user_node, user_secret, user_status, mobile_no, email_id, ins_medium)
select 'The Secretary' user_name, 2 user_type, node_id user_node, 
concat(a.col_code, LEFT(md5(rand()), 3)) user_secret, 1 user_status, null mobile_no, null email_id, 'E' ins_medium
from cl_node_mst a;

update cl_user_mst x
set mobile_no = (select mobile_no from il_node_contact y where left(x.user_secret,5) = y.col_code)
where exists (select 1 from il_node_contact y where left(x.user_secret,5) = y.col_code)
and x.mobile_no is null
and x.user_type = 2;

select mobile_no, concat('Dear Secretary, ',b.node_name, ', Yoor access code for D.El.Ed Part II Online Classes is ', user_secret, '. Click here http://edu.educlasses.in/access?usercode=', user_secret) sms
from cl_user_mst a, cl_node_mst b
where a.user_node = b.node_id
and a.mobile_no is not null
and a.user_type = 2;

update cl_user_mst set user_secret = concat(user_secret, 'N') where user_type = 2;

grant select, show view on eduDB.* to userImporter;
grant insert, update, delete on eduDB.il_user_import to userImporter;
grant insert, update, delete on eduDB.il_node_contact to userImporter;
grant all privileges on eduDB.* to souvik;
grant insert, update, delete on eduDB.IL_BANK_STATEMENT to userImporter;
GRANT INSERT, UPDATE, DELETE ON eduDB.IL_BANK_STATEMENT_DTL TO userImporter;
grant insert, update, delete on eduDB.il_mob_mail_upd to userImporter;

COMMIT;

ALTER TABLE il_user_attendance  DROP FOREIGN KEY il_user_attendance_ibfk_1;

ALTER TABLE il_user_attendance ADD CONSTRAINT il_user_attendance_ibfk_1
FOREIGN KEY (user_id) REFERENCES cl_user_mst(user_id);

COMMIT;
var express = require ('express');
var favicon = require('serve-favicon');
var prometheus = require('prometheus-api-metrics');
var cron = require("node-cron");

var constants = require('./app/constants');
var dbops = require('./app/dbops');

var app = express();
var port = 8081;

// View Engine
app.set('view engine', 'html');
app.engine('html', require('ejs').renderFile);

// lets connect the routs file
var router = require ('./app/routs');
app.use ('/', router); // use is a middleware function with access to all req and res objects


// location of static contents
app.use (express.static(__dirname + "/public"));

// icoc
app.use (favicon(__dirname + '/public/images/icon.ico'));

// Prometheus metrices
app.use(prometheus());

// Use a cron to hit the DB every 30 mins
cron.schedule("*/15 * * * *", () => {
	dbops.assessClassTime();
	dbops.pingDB().then((data) => {
		console.log("DB Ping Success", data, new Date());
	}).catch((err) => {
		console.log("DB Ping Error", err, new Date());
	}).then(() => {
		dbops.assessClassTime();
	});
});

// Start the server
app.listen (process.env.PORT || port, function() {
	dbops.handleDisconnect();
	dbops.getUiMetaData().then((data) => {
		console.log (constants.getMetaData());
		dbops.assessClassTime();
		console.log("app started");
	}).catch((err) => {
		console.log (err);
	});
  	console.log ("app starting");
});
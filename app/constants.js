

let classTitle = '';
let videoLink = '';
let videoDesc = '';
let videoStartTime = '';
let videoEndTime = '';
let videoDate = '';
let nextClass = '';

let defVideoTitle = '';
let defVideoLink = '';
let defVideodesc = '';

let scheduleMarkup = '';

let assignmentId = 0;
let assignmentCount = 1;
let assignmentQuestions = '';
let assignmentOpenTime = '';
let assignmentCloseTime = '';

let archiveVideos = [];

let classTime = 0; // 0: class not started, 1: class is on

module.exports.setclassTitle = (val) => classTitle = val;
module.exports.setVideoLink = (val) => videoLink = val;
module.exports.setVideoDesc = (val) => videoDesc = val;
module.exports.setVideoStartTime = (val) => videoStartTime = val;
module.exports.setVideoEndTime = (val) => videoEndTime = val;
module.exports.setVideoDate = (val) => videoDate = val;
module.exports.setNextClass = (val) => nextClass = val;

module.exports.setDefVideoTitle = (val) => defVideoTitle = val;
module.exports.setDefVideoLink = (val) => defVideoLink = val;
module.exports.setDefVideoDesc = (val) => defVideodesc = val;

module.exports.setScheduleMarkup = (val) => scheduleMarkup = val;

module.exports.setAssignmentId = (val) => assignmentId = val;
module.exports.setAssignmentCount = (val) => assignmentCount = val;
module.exports.setAssignmentQuestions = (val) => assignmentQuestions = val;
module.exports.setAssignmentOpenTime = (val) => assignmentOpenTime = val;
module.exports.setAssignmentCloseTime = (val) => assignmentCloseTime = val;

module.exports.setArchiveVideos = (val) => archiveVideos = val;

module.exports.setClassTime = () => classTime = 1;
module.exports.unsetClassTime = () => classTime = 0;

module.exports.getMetaData = () => {
	return {
		class: classTitle,
		vlink: videoLink,
		vdesc: videoDesc,
		vstart: videoStartTime,
		vend: videoEndTime,
		vdt: videoDate,
		next: nextClass,
		dvtitle: defVideoTitle,
		dvlink: defVideoLink,
		dvdesc: defVideodesc,
		schedule: scheduleMarkup,
		aid: assignmentId,
		acount: assignmentCount,
		atext: assignmentQuestions,
		astart: assignmentOpenTime,
		aend: assignmentCloseTime,
		archv: archiveVideos,
		time: classTime
	};
};

module.exports.getMonths = () => {
	return ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
};

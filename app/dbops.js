var mysql = require('mysql');

var PropertiesReader = require('properties-reader');
var properties = new PropertiesReader('./secrets/dbinfo');
var constants = require('./constants');
var storage = require('./storage');

let dbConfigs = {
	connectionLimit : properties._properties.POOL_SIZE,
 	host: properties._properties.DB_HOST,
 	port: properties._properties.DB_PORT,
 	user: properties._properties.DB_USER,
 	password: properties._properties.DB_PASS,
 	database: properties._properties.DB_NAME
};

var con = mysql.createConnection(dbConfigs);

getArchiveVedeoQuery = () => {
	// Phase 1: Link_id <= 60
	// Phase 2: Link_id 61 - 113: Dated - 05.02.2021
	let archiveVidQuery = "select class_title, link, video_info, date_format(created_at, '%d-%m-%Y') class_date";
	archiveVidQuery += " from ui_video_link where link_id in";
	archiveVidQuery += " (select max(link_id) from ui_video_link where link_id > 60 group by link)";
	archiveVidQuery += " order by created_at desc";
	return archiveVidQuery;
}

getAttendanceQuery = () => {
	let insQuery = "insert into il_user_attendance (user_id)";
	insQuery += " select user_id from cl_user_mst a";
	insQuery += " where a.user_id = ?";
	insQuery += " and not exists (select 1 from il_user_attendance b";
	insQuery += " where a.user_id = b.user_id";
	insQuery += " and cast(b.login_time as date) = cast(current_timestamp as date))";
	return insQuery;
};

module.exports.handleDisconnect = () => {
	con.on("error", (err) => {
		console.log('db error');
		console.log(err);
		if (err.code === 'PROTOCOL_CONNECTION_LOST') {
			con = mysql.createConnection(dbConfigs);
		} else {
			throw err;
		}
	});
}

module.exports.attendance = (userId, results) => {
	return new Promise ((resolve, reject) => {

		
		con.query(insQuery, results[0].user_id, function(err, result) {
			if (err)	reject (err);
			else		resolve(results);
		});
	});
};

module.exports.getUserDetails = (userCode) => {
	let query = "select m.user_id, user_name, t.type_desc, n.node_name, t.type_id, n.node_id, n.col_code";
	query += " from cl_user_mst m";
	query += " inner join cl_user_type t on t.type_id = m.user_type";
	query += " inner join cl_user_status s on s.status_id = m.user_status";
	query += " inner join cl_node_mst n on n.node_id = m.user_node";
	query += " where m.user_secret = ? and m.user_status = 1"; 

	return new Promise ((resolve, reject) => {
		con.query(query, userCode, function(err, results, fields) {
			if (err)	reject(err);
			else		resolve(results);
		});
	}).then((results) => {
		return new Promise ((resolve, reject) => {
			if (results.length !== 1)	reject (results.length + ' users present with code ' + userCode);
			else if (constants.getMetaData().time == 1) {
				con.query(getAttendanceQuery(), results[0].user_id, function(err, result) {
					if (err)	reject (err);
					else		resolve(results);
				});
			} else {
				resolve(results);
			}
		});
	});
};

module.exports.markAttendance = (userId) => {
	return new Promise ((resolve, reject) => {
		if (constants.getMetaData().time == 1) {
			con.query(getAttendanceQuery(), userId, function(err, result) {
				if (err)	reject (err);
				else		resolve(true);
			});
		} else {
			resolve(true);
		}
	});
}

module.exports.addAssignment = (userid, filePath, originalName, modifiedName, userNode) => {
	return new Promise ((resolve, reject) => {
		con.beginTransaction((err) => {
			if (err)	reject (err);
			else		resolve();
		});
	}).then(() => {
		return new Promise ((resolve, reject) => {
			let query = "insert into il_user_assignment (user_id, original_name, modified_name) values (?, ?, ?)";
			con.query(query, [userid, originalName, modifiedName], function(err, result) {
				if (err)	reject (err);
				else		resolve(result.affectedRows);
			});
		});	
	}).then((count) => {
		return storage.uploadToS3(filePath, modifiedName, userNode, constants.aid);
	}).then((data) => {
		return new Promise ((resolve, reject) => {
			con.commit(function(err) {
				if (err)	reject (err);
				else		resolve(data);
			});
		});
	}).catch((err) => {
		return new Promise ((resolve, reject) => {
			con.rollback(function(dberr) {
				if (dberr)	console.log(dberr);
				reject(err);
			});
		});
	});
};

module.exports.getUiMetaData = () => {
	return new Promise ((resolve, reject) => {
		let videoLinkQuery = "select class_title, link, video_info, start_time_str, end_time_str, date_format(video_date, '%d/%m/%Y') video_date, date_format(next_class, '%d/%m/%Y') next_class from ui_video_link where is_active = 'Y'";
		con.query(videoLinkQuery, function(err, results, fields) {
			if (err)	reject(err);
			else if (results.length !== 1)	reject ('Video information not available');
			else {
				let data = {};
				data.class = results[0].class_title
				data.vlink = results[0].link;
				data.vdesc = results[0].video_info;
				data.vstart = results[0].start_time_str;
				data.vend = results[0].end_time_str;
				data.vdt = results[0].video_date;
				data.next = results[0].next_class;
				resolve(data);
			}
		});
	}).then((data) => {
		return new Promise ((resolve, reject) => {
			let defVideoQuery = "select class_title, link, video_info from ui_video_link where is_default = 'Y'";
			con.query(defVideoQuery, function(err, results, fields) {
				if (err)	reject(err);
				else if (results.length !== 1)	reject ('default Video information not available');
				else {
					data.dvtitle = results[0].class_title;
					data.dvlink = results[0].link;
					data.dvdesc = results[0].video_info;
					resolve(data);
				}
			});
		});
	}).then((data) => {
		let scheduleQuery = "select markup from ui_schedule_txt where is_active = 'Y'";
		return new Promise ((resolve, reject) => {
			con.query(scheduleQuery, function(err, results, fields) {
				if (err)	reject(err);
				else if (results.length !== 1)	resolve(data);
				else {
					data.schedule = results[0].markup;
					resolve(data);
				}
			});
		});
	}).then((data) => {
		let assgnQuery = "select assignment_id, assignment_count, assignment_text, date_format(upload_open_date, '%Y-%m-%dT%H:%i') start_date,";
		assgnQuery += " date_format(upload_close_date, '%Y-%m-%dT%H:%i') end_date from ui_assignment_txt where is_active = 'Y'";
		return new Promise ((resolve, reject) => {
			con.query(assgnQuery, function(err, results, fields) {
				if (err)	reject(err);
				else if (results.length !== 1)	resolve(data);
				else {
					data.aid = results[0].assignment_id;
					data.acount = results[0].assignment_count;
					data.atext = results[0].assignment_text;
					data.astart = results[0].start_date;
					data.aend = results[0].end_date;
					resolve(data);
				}
			});
		});
	}).then((data) => {
		let archiveVidQuery = getArchiveVedeoQuery();
		return new Promise ((resolve, reject) => {
			con.query(archiveVidQuery, function(err, results, fields) {
				if (err)	reject(err);
				else {
					data.archv = results;
					resolve(data);
				}
			});
		});
	}).then((data) => {
		return new Promise ((resolve, reject) => {
			constants.setclassTitle(data.class);
			constants.setVideoLink(data.vlink);
			constants.setVideoDesc(data.vdesc);
			constants.setVideoStartTime(data.vstart);
			constants.setVideoEndTime(data.vend);
			constants.setVideoDate(data.vdt);
			constants.setNextClass(data.next);

			constants.setDefVideoTitle(data.dvtitle);
			constants.setDefVideoLink(data.dvlink);
			constants.setDefVideoDesc(data.dvdesc);
			
			constants.setScheduleMarkup(data.schedule);
			
			constants.setAssignmentId(data.aid);
			constants.setAssignmentCount(data.acount);
			constants.setAssignmentQuestions(data.atext);
			constants.setAssignmentOpenTime(data.astart);
			constants.setAssignmentCloseTime(data.aend);
			
			constants.setArchiveVideos(data.archv);
			
			resolve(data);
		});
	});	
};

module.exports.updateVideoMetaData = (cls, link, desc, startTime, endTime, classDate, next, user) => {
	return new Promise ((resolve, reject) => {
		con.beginTransaction((err) => {
			if (err)	reject (err);
			else		resolve({cls: cls, link: link, desc:desc, user: user, startTime: startTime, endTime: endTime, cdate: classDate, next: next});
		});
	}).then ((data) => {
		let updateVideoDataQuery = "update ui_video_link set is_active = 'N' where is_active = 'Y'";
		return new Promise ((resolve, reject) => {
			con.query(updateVideoDataQuery, function(err, result) {
				if (err)	reject (err);
				else 		resolve(data);
			});
		});
	}).then((data) => {
		let insertVideoDataQuery = "insert into ui_video_link (link, video_info, is_active, creator_secret, class_title, start_time_str, end_time_str, video_date, next_class) values (?, ?, ?, ?, ?, ?, ?, ?, ?) ";
		return new Promise ((resolve, reject) => {
			con.query(insertVideoDataQuery, [data.link, data.desc, 'Y', data.user, data.cls, data.startTime, data.endTime, data.cdate, data.next], function(err, result) {
				if (err)	reject (err);
				else		resolve(result.affectedRows);
			});
		});
	}).then((count) => {
		return new Promise ((resolve, reject) => {
			con.commit(function(err) {
				if (err)	reject (err);
				else		resolve(count);
			});
		});
	}).then((count) => {
		let query = "select date_format(video_date, '%d/%m/%Y') video_date, date_format(next_class, '%d/%m/%Y') next_class from ui_video_link where is_active = 'Y'";
		return new Promise ((resolve, reject) => {
			con.query(query, function(err, results) {
				if (err)	reject (err);
				else 		resolve(results);
			});
		});
	}).then((data) => {
		let nextP = new Date().toLocaleDateString();
		let vdate = new Date().toLocaleDateString();
		if (data.length === 1) {
			nextP = data[0].next_class;
			vdate = data[0].video_date;
		}
		return new Promise ((resolve, reject) => {
			constants.setclassTitle(cls);
			constants.setVideoLink(link);
			constants.setVideoDesc(desc);
			constants.setVideoStartTime(startTime);
			constants.setVideoEndTime(endTime);
			constants.setVideoDate(vdate);
			constants.setNextClass(nextP);
			resolve(true);
		});
	}).then((flag) => {
		let archiveVidQuery = getArchiveVedeoQuery();
		return new Promise ((resolve, reject) => {
			con.query(archiveVidQuery, function(err, results, fields) {
				if (err)	reject(err);
				else {
					constants.setArchiveVideos(results);
					resolve(true);
				}
			});
		});
	}).catch((err) => {
		return new Promise ((resolve, reject) => {
			con.rollback(function(dberr) {
				if (dberr)	console.log(dberr);
				reject(err);
			});
		});
	});
};

module.exports.updateScheduleMetaData = (markup, user) => {
	return new Promise ((resolve, reject) => {
		con.beginTransaction((err) => {
			if (err)	reject (err);
			else		resolve({markup: markup, user: user});
		});
	}).then ((data) => {
		let updateVideoDataQuery = "update ui_schedule_txt set is_active = 'N' where is_active = 'Y'";
		return new Promise ((resolve, reject) => {
			con.query(updateVideoDataQuery, function(err, result) {
				if (err)	reject (err);
				else 		resolve(data);
			});
		});
	}).then((data) => {
		let insertVideoDataQuery = "insert into ui_schedule_txt (markup, is_active, creator_secret) values (?, ?, ?) ";
		return new Promise ((resolve, reject) => {
			con.query(insertVideoDataQuery, [data.markup, 'Y', data.user], function(err, result) {
				if (err)	reject (err);
				else		resolve(result.affectedRows);
			});
		});
	}).then((count) => {
		return new Promise ((resolve, reject) => {
			con.commit(function(err) {
				if (err)	reject (err);
				else		resolve(count);
			});
		});
	}).then((count) => {
		return new Promise ((resolve, reject) => {
			constants.setScheduleMarkup(markup);
			resolve(true);
		});
	}).catch((err) => {
		return new Promise ((resolve, reject) => {
			con.rollback(function(dberr) {
				if (dberr)	console.log(dberr);
				reject(err);
			});
		});
	});
};

module.exports.updateAssignmentMetaData = (acount, text, astart, aend, user) => {
	return new Promise ((resolve, reject) => {
		con.beginTransaction((err) => {
			if (err)	reject (err);
			else		resolve({acount: acount, text: text, astart: astart, aend: aend, user: user});
		});
	}).then ((data) => {
		let updateVideoDataQuery = "update ui_assignment_txt set is_active = 'N' where is_active = 'Y'";
		return new Promise ((resolve, reject) => {
			con.query(updateVideoDataQuery, function(err, result) {
				if (err)	reject (err);
				else 		resolve(data);
			});
		});
	}).then((data) => {
		let insertVideoDataQuery = "insert into ui_assignment_txt (assignment_count, assignment_text, is_active, creator_secret, upload_open_date, upload_close_date) values (?, ?, ?, ?, ?, ?) ";
		return new Promise ((resolve, reject) => {
			con.query(insertVideoDataQuery, [data.acount, data.text, 'Y', data.user, data.astart, data.aend], function(err, result) {
				if (err)	reject (err);
				else		resolve(result.affectedRows);
			});
		});
	}).then((count) => {
		return new Promise ((resolve, reject) => {
			con.commit(function(err) {
				if (err)	reject (err);
				else		resolve(count);
			});
		});
	}).then((count) => {
		return new Promise ((resolve, reject) => {
			constants.setAssignmentCount(acount);
			constants.setAssignmentQuestions(text);
			constants.setAssignmentOpenTime(astart);
			constants.setAssignmentCloseTime(aend)
			resolve(true);
		});
	}).catch((err) => {
		return new Promise ((resolve, reject) => {
			con.rollback(function(dberr) {
				if (dberr)	console.log(dberr);
				reject(err);
			});
		});
	});
};

module.exports.getAttandancesNode = (type, colCode, node, date) => {
	if (type !== 1 && colCode !== node.split(':')[0]) {
		return new Promise((resolve, reject) => {
			reject ("Unauthorized Access");
		});
	} else {
		let query = "select login_time, user_name from il_user_attendance a";
		query += " inner join cl_user_mst b on a.user_id = b.user_id and b.user_type= 3";
		query += " and b.user_node =  (select node_id from cl_node_mst where col_code = ?)";
		query += " where cast(login_time as date) = ? order by user_name";
		return new Promise((resolve, reject) => {
			con.query(query, [node.split(':')[0], date], function(err, results) {
				if (err)	reject (err);
				else		resolve(results);
			});
		});	
	}
};

module.exports.getRegisteredUsers = (type, colCode, node) => {
	if (type !== 1 && colCode !== node.split(':')[0]) {
		return new Promise((resolve, reject) => {
			reject ("Unauthorized Access");
		});
	} else {
		return new Promise((resolve, reject) => {
			let query = "select m.user_name, m.user_secret, t.type_desc, m.mobile_no, m.email_id, m.ins_medium";
			query += " from cl_user_mst m inner join cl_user_type t on m.user_type = t.type_id"; 
			query += " and user_node = (select node_id from cl_node_mst where col_code = ?) and m.user_type= 3";
			con.query(query, node.split(':')[0], function(err, results) {
				if (err)	reject (err);
				else		resolve(results);
			});
		});
	}
};

module.exports.getNodes = (userType, userNode) => {
	if (userType == 1) {
		let query = "select node_id, node_name, col_code from cl_node_mst";
		return new Promise((resolve, reject) => {
			con.query(query, function(err, results) {
				if (err)	reject (err);
				else		resolve(results);
			});
		});
	} else {
		let query = "select node_id, node_name, col_code from cl_node_mst where node_id = ?";
		return new Promise((resolve, reject) => {
			con.query(query, userNode, function(err, results) {
				if (err)	reject (err);
				else		resolve(results);
			});
		});
	}
};

module.exports.pingDB = () => {
	return new Promise((resolve, reject) => {
		con.query("select status_code from cl_app_status", function(err, results) {
			if (err)	reject (err);
			else		resolve(results);
		});
	});
};

module.exports.getUploadCountReport = () => {
	let query ="select a.*, round(b.payment/500, 0) students , b.payment from (";
	query += " select SUM(cast(replace(replace(bank_amount,'.00',''),',','') as UNSIGNED )) payment, col_code ";
	query += " from IL_BANK_STATEMENT group by col_code) b  right join (";
	query += " select i.college_id, m.district, node_name, count(i.student_code) uploaded_count, m.intake";
	query += " from il_user_import i, cl_node_mst m where i.college_id = m.col_code";
	query += " group by i.college_id, m.col_code) a on a.college_id = b.col_code order by a.college_id";
	return new Promise((resolve, reject) => {
		con.query(query, function(err, results) {
			if (err)	reject (err);
			else		resolve(results);
		});
	});
};

module.exports.importUsers = () => {
	return new Promise ((resolve, reject) => {
		con.beginTransaction((err) => {
			if (err)	reject (err);
			else		resolve(true);
		});
	}).then((data) => {
		return new Promise ((resolve, reject) => {
			con.query("drop temporary table if exists temp_college_id", function(err, results) {
				if (err)	reject (err);
				else 		resolve(true);
			});
		});
	}).then((data) => {
		return new Promise ((resolve, reject) => {
			let query = "create temporary table temp_college_id select a.college_id from";
			query += " (select college_id, count(*) * 500 expected from il_user_import group by college_id ) a,";
			query += " (select col_code, sum(COALESCE(confirm_bank_amount,0)) received from IL_BANK_STATEMENT where col_code not like 'Z%' group by col_code) b";
			query += " where a.college_id = b.col_code and (a.expected - b.received) <= 1000"; // 100 rs grace
			con.query(query, function(err, results) {
				console.log("Selected node for payment", (results ? results.affectedRows : 'err'));
				if (err)	reject (err);
				else 		resolve(results.affectedRows);
			});
		});
	}).then((data) => {
		return new Promise ((resolve, reject) => {
			let query = "insert into temp_college_id (college_id) select a.college_id from";
			query += " (select college_id, count(*) * 500 expected from il_user_import group by college_id ) a,";
			query += " (select col_code, sum(COALESCE(confirm_bank_amount,0)) received from IL_BANK_STATEMENT_DTL where col_code not like 'Z%' group by col_code) b";
			query += " where a.college_id = b.col_code and (a.expected - b.received) <= 1000"; // 100 rs grace
			con.query(query, function(err, results) {
				console.log("Selected node for payment from dtl", (results ? results.affectedRows : 'err'));
				if (err)	reject (err);
				else 		resolve(data + results.affectedRows);
			});
		});
	}).then((data) => {
		return new Promise ((resolve, reject) => {
			let query = "update cl_node_mst x set x.payment_confirmed = CURRENT_TIMESTAMP";
			query += " where exists (select 1 from temp_college_id y where x.col_code = y.college_id)";
			query += " and x.payment_confirmed is null";
			con.query(query, function(err, results) {
				console.log("Updated node for payment", (results ? results.affectedRows : 'err'));
				if (err)	reject (err);
				else {
					let updated = results.affectedRows;				
					if (data < updated)	reject ("count mismatch [1] [" + data + "/" + updated + "]");
					else					resolve(true);
				}
			});
		});
	}).then((data) => {
		return new Promise ((resolve, reject) => {
			let query = "insert into cl_user_mst ";
			query += " (user_name, user_type, user_node, user_secret, user_status, mobile_no, email_id, ins_medium)";
			query += " select user_name, 3 user_type, b.node_id user_node, concat(college_id, lpad(student_code, 3, '0')) user_secret,";
			query += " 1 user_status, a.mobile_no, a.mainl_id email_id, ins_medium";
			query += " from il_user_import a, cl_node_mst b";
			query += " where a.college_id = b.col_code and b.payment_confirmed is not null and a.import_date is null";
			con.query(query, function(err, results) {
				console.log("Imported users to CL", (results ? results.affectedRows : 'err'));
				if (err)	reject (err);
				else 		resolve(results.affectedRows);
			});
		});
	}).then((data) => {
		return new Promise ((resolve, reject) => {
			let query = "update il_user_import x set x.import_date = current_timestamp";
			query += " where exists (select 1 from cl_user_mst y where x.college_id = substr(y.user_secret,1,6)";
			query += " and concat(x.college_id, lpad(x.student_code, 3, '0')) = y.user_secret)";
			query += " and x.import_date is null";
			con.query(query, function(err, results) {
				console.log("Updated users imported", (results ? results.affectedRows : 'err'));
				if (err)	reject (err);
				else {
					let updated = results.affectedRows;				
					if (data !== updated)	reject ("count mismatch [2] [" + data + "/" + updated + "]");
					else					resolve(true);
				}
			});
		});
	}).then((data) => {
		return new Promise ((resolve, reject) => {
			let query = "update cl_user_mst x set x.mobile_no = (select y.new_mobile from il_mob_mail_upd y where y.user_code = x.user_secret and y.cl_update_date is null)";
			query += " where exists (select 1 from il_mob_mail_upd z where z.user_code = x.user_secret and z.cl_update_date is null and trim(z.new_mobile) is not null)";
			con.query(query, function(err, results) {
				console.log("Updated user mobile", (results ? results.affectedRows : 'err'));
				if (err)	reject (err);
				else 		resolve(true);
			});
		});
	}).then((data) => {
		return new Promise ((resolve, reject) => {
			let query = "update cl_user_mst x set x.email_id = (select y.new_mail from il_mob_mail_upd y where y.user_code = x.user_secret and y.cl_update_date is null)";
			query += " where exists (select 1 from il_mob_mail_upd z where z.user_code = x.user_secret and z.cl_update_date is null and trim(z.new_mail) is not null)";
			con.query(query, function(err, results) {
				console.log("Updated user mail", (results ? results.affectedRows : 'err'));
				if (err)	reject (err);
				else 		resolve(true);
			});
		});
	}).then((data) => {
		return new Promise ((resolve, reject) => {
			let query = "update il_mob_mail_upd y set y.cl_update_date = current_timestamp where y.cl_update_date is null";
			con.query(query, function(err, results) {
				console.log("mark updation done", (results ? results.affectedRows : 'err'));
				if (err)	reject (err);
				else 		resolve(true);
			});
		});
	}).then((data) => {
		return new Promise ((resolve, reject) => {
			let welcomeSmsTime = new Date().getTime();
			let query = "update cl_user_mst set welcome_sms_date = ? where welcome_sms_date is null and user_type = 3";
			con.query(query, welcomeSmsTime, function(err, results) {
				console.log("Marked users welcome", (results ? results.affectedRows : 'err'));
				if (err)	reject (err);
				else		resolve(welcomeSmsTime);
			});
		});
	}).then((data) => {
		return new Promise ((resolve, reject) => {
			let query = "select mobile_no, user_name, user_secret from cl_user_mst where welcome_sms_date = ? and user_type = 3";
			con.query(query, data, function(err, results) {
				console.log("Selected users for sms", (results ? results.length : 'err'));
				if (err)	reject (err);
				else		resolve(results);
			});
		});
	}).then((data) => {
		return new Promise ((resolve, reject) => {
			con.commit(function(err) {
				if (err)	reject (err);
				else		resolve(data);
			});
		});
	}).catch((perr) => {
		return new Promise ((resolve, reject) => {
			con.rollback(function(dberr) {
				if (dberr)	console.log(dberr);
				reject(perr);
			});
		});
	});
};

module.exports.nodalSMS = () => {
	return new Promise ((resolve, reject) => {
		con.beginTransaction((err) => {
			if (err)	reject (err);
			else		resolve(true);
		});
	}).then((data) => {
		return new Promise ((resolve, reject) => {
			let query = "update cl_user_mst x set mobile_no = (select mobile_no from il_node_contact y where left(x.user_secret,6) = y.col_code)";
			query += " where exists (select 1 from il_node_contact y where left(x.user_secret,6) = y.col_code) and x.mobile_no is null and x.user_type = 2";
			con.query(query, function(err, results) {
				console.log("Updated Node mobile", (results ? results.affectedRows : 'err'));
				if (err)	reject (err);
				else		resolve(true);
			});
		});
	}).then((data) => {
		return new Promise ((resolve, reject) => {
			let query = "update cl_user_mst x set email_id = (select email_id from il_node_contact y where left(x.user_secret,6) = y.col_code)";
			query += " where exists (select 1 from il_node_contact y where left(x.user_secret,6) = y.col_code) and x.email_id is null and x.user_type = 2";
			con.query(query, function(err, results) {
				console.log("Updated Node mail", (results ? results.affectedRows : 'err'));
				if (err)	reject (err);
				else		resolve(true);
			});
		});
	}).then((data) => {
		return new Promise ((resolve, reject) => {
			let welcomeSmsTime = new Date().getTime();
			let query = "update cl_user_mst x set x.welcome_sms_date = ? where x.welcome_sms_date is null and x.user_type = 2 and x.mobile_no is not null";
			query += " and exists (select 1 from cl_node_mst y where x.user_node = y.node_id and y.payment_confirmed is not null)";
			con.query(query, welcomeSmsTime, function(err, results) {
				console.log("Marked Nodes welcome", (results ? results.affectedRows : 'err'));
				if (err)	reject (err);
				else		resolve(welcomeSmsTime);
			});
		});
	}).then((data) => {
		return new Promise ((resolve, reject) => {
			let query = "select a.mobile_no, b.node_name, user_secret from cl_user_mst a, cl_node_mst b where a.user_node = b.node_id and a.welcome_sms_date = ? and a.user_type = 2";
			con.query(query, data, function(err, results) {
				console.log("Selected nodes for sms", (results ? results.length : 'err'));
				if (err)	reject (err);
				else		resolve(results);
			});
		});
	}).then((data) => {
		return new Promise ((resolve, reject) => {
			con.commit(function(err) {
				if (err)	reject (err);
				else		resolve(data);
			});
		});
	}).catch((err) => {
		return new Promise ((resolve, reject) => {
			con.rollback(function(dberr) {
				if (dberr)	console.log(dberr);
				reject(err);
			});
		});
	});
};

module.exports.paymentReport = () => {
	let query = "select g.dist_code, g.dist_name, coalesce(g.count_college,0) count_college, coalesce(g.count_student,0) count_student, coalesce(g.count_paid_college,0) count_paid_college,";
	query += "  coalesce(g.amt_recieved,0) amt_recieved, coalesce(h.unclaimed_amt_recieved,0) unclaimed_amt_recieved";
	query += " from";
	query += " (";
	query += " select 'UU' dist_code, SUM(cast(replace(replace(bank_amount,'.00',''),',','') as UNSIGNED )) unclaimed_amt_recieved";
	query += " from `IL_BANK_STATEMENT` where PART_PAYMENT_SRL = 0 and cast(replace(replace(bank_amount,'.00',''),',','') as UNSIGNED ) >= 500";
	query += " ) h ";
	query += " right outer join";
	query += " (";
	query += " 	select e.dist_code, e.dist_name, e.count_college, e.count_student, f.count_paid_college, f.amt_recieved";
	query += " 	from";
	query += " 	(";
	query += " 	select dist_code, sum(paid_college) count_paid_college, sum(amt_recieved) amt_recieved";
	query += "  from ("
	query += "  	select substring(col_code, 1, 2) dist_code, count(distinct col_code) paid_college, sum(cast(CONFIRM_BANK_AMOUNT  as unsigned)) amt_recieved";
	query += "  	from IL_BANK_STATEMENT where PART_PAYMENT_SRL <> 0 and COL_CODE not like 'Z%' group by  substring(col_code, 1, 2)";
	query += "  	union all";
	query += "  	select substring(col_code, 1, 2) dist_code, count(distinct col_code) paid_college, sum(cast(CONFIRM_BANK_AMOUNT  as unsigned)) amt_recieved";
	query += "  	from IL_BANK_STATEMENT_DTL group by  substring(col_code, 1, 2)";
	query += "  	) fc group by dist_code";
	query += " 	) f";
	query += " 	right outer join";
	query += " 	(";
	query += " 		select c.dist_code, c.dist_name, c.count_college, d.count_student from";
	query += " 		(";
	query += " 		select substring(college_id, 1, 2) dist_code, count(*) count_student";
	query += " 		from il_user_import group by  substring(college_id, 1, 2) ";
	query += " 		) d";
	query += " 		right outer join";
	query += " 		(";
	query += " 			select  b.dist_code, b.dist_name, a.count_college from";
	query += " 			(";
	query += " 			select substring(college_id, 1, 2) dist_code, count(distinct college_id) count_college";
	query += " 			from il_user_import group by  substring(college_id, 1, 2) ";
	query += " 			) a";
	query += " 			right outer join";
	query += " 			(";
	query += " 			select dist_code, dist_name from cl_dist_code_mst mm";
	query += " 			) b";
	query += " 			on a.dist_code = b.dist_code";
	query += " 		) c";
	query += " 		on c.dist_code = d.dist_code";
	query += " 	) e";
	query += " 	on e.dist_code = f.dist_code";
	query += " ) g";
	query += " on h.dist_code = g.dist_code";
	query += " order by 1";
	return new Promise((resolve, reject) => {
		con.query(query, function(err, results) {
			if (err)	reject (err);
			else		resolve(results);
		});
	});
};

module.exports.assessClassTime = () => {
	try {
		let videoDate = constants.getMetaData().vdt;

		let classStartTimeHour = constants.getMetaData().vstart.split(':')[0];
		let classStartTimeMin = constants.getMetaData().vstart.split(':')[1];
		let classStart = 60 * parseInt(classStartTimeHour) + parseInt(classStartTimeMin);

		let classEndTimeHour = constants.getMetaData().vend.split(':')[0];
		let classEndTimeMin = constants.getMetaData().vend.split(':')[1];
		let classEnd = 60 * parseInt(classEndTimeHour) + parseInt(classEndTimeMin);

		let currentTimeStamp = new Date();
		let currentHours = currentTimeStamp.getHours();
		let currentMin = currentTimeStamp.getMinutes();
		let currentTme = 60 * parseInt(currentHours) + parseInt(currentMin);

		componntsVdt = videoDate.split('/'); // 07/12/2020 
		componntsNow = new Date().toLocaleDateString().split('/'); // 12/7/2020

		let ddVdt = parseInt(componntsVdt[0]);
		let mmVdt = parseInt(componntsVdt[1]);
		let yyVdt = parseInt(componntsVdt[2]);

		let ddNow = parseInt(componntsNow[1]);
		let mmNow = parseInt(componntsNow[0]);
		let yyNow = parseInt(componntsNow[2]);

		if (ddVdt === ddNow && mmVdt === mmNow && yyVdt === yyNow && currentTme <= classEnd && currentTme >= classStart) {
			constants.setClassTime();
			console.log(videoDate, classStart, classEnd);
			console.log(new Date().toLocaleDateString(), currentHours, ":", currentMin, "class time");
		} else {
			constants.unsetClassTime();
			console.log(videoDate, classStart, classEnd);
			console.log(new Date().toLocaleDateString(), currentHours, ":", currentMin, "not class time");
		}
	} catch (err) {
		console.log (err);
	}
};
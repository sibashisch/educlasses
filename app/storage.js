const fs = require('fs');
const AWS = require('aws-sdk');

var PropertiesReader = require('properties-reader');
var properties = new PropertiesReader('./secrets/s3info');

const s3 = new AWS.S3({
  accessKeyId: properties._properties.AWS_ACCESS_KEY,
  secretAccessKey: properties._properties.AWS_SECRET_ACCESS_KEY
});

let s3BucketName = properties._properties.AWS_S3_BUCKET;

module.exports.uploadToS3 = (originalFilePath, modifiedFileName, nodeName, asgnId) => {
	return new Promise ((resolve, reject) => {
		fs.readFile(originalFilePath, (err, data) => {
			if (err)	reject (err);
			else 		resolve(data);
		});
	}).then((data) => {
		let date = new Date().toISOString().split("T")[0];
		let fileKey = [nodeName, date, asgnId, modifiedFileName].join('/');
		return new Promise((resolve, reject) => {
			s3.putObject({Bucket: s3BucketName, Key: fileKey, Body: data}, (err, data) => {
				if (err)	reject (err);
				else		resolve(data);
			});
		});
	});
};
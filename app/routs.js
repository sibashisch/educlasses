var express = require ('express');
var path = require ('path');	// required for sending files over get
var bodyParser = require ('body-parser');
var session = require ('express-session');
var formidable = require('formidable');
var mySQLSessionStore = require('express-mysql-session')(session);
var PropertiesReader = require('properties-reader');

var constants = require('./constants');
var dbops = require('./dbops');

var router = express.Router();

// export the router so that we can use it in server.js
module.exports = router; 

// use the bodyparser to parse url data to json
router.use(bodyParser.urlencoded({extended: true}));
router.use(bodyParser.json());

// mySQL Session Store options
var properties = new PropertiesReader('./secrets/dbinfo');
var options = {
    host: properties._properties.DB_HOST,
    port: properties._properties.DB_PORT,
    user: properties._properties.DB_USER,
    password: properties._properties.DB_PASS,
    database: properties._properties.DB_NAME
};
var sessionStore = new mySQLSessionStore(options);

// use the session functionality
router.use(session({key: 'eduClassesKey', secret: 'eduClassesSecret', store: sessionStore, resave: false, saveUninitialized: false}));

// Common functions used in this file

// the login handler
loginHandler = (userCode, req, res) => {
	if (!userCode) {
		res.render("pages/login.html");
	}
	dbops.getUserDetails(userCode).then((result) => {
		if (result.length !== 1) {
			console.log("No User Found with UserCode = " + userCode);
			res.render("pages/login.html", {message: 'Invalid User Code.'});
		} else {
			let sess = req.session;
			sess.userId = result[0].user_id;
			sess.userCode = userCode;
			sess.userName = result[0].user_name;
			sess.userType = result[0].type_desc;
			sess.userNode = result[0].node_name;
			sess.userTypeId = result[0].type_id;
			sess.userNodeId = result[0].node_id;
			sess.userColCode = result[0].col_code;
			res.render("pages/landing.html", {name: sess.userName, type: sess.userTypeId, code: sess.userCode, node: sess.userNode, metadata: constants.getMetaData()});
		}
	}).catch((err) => {
		console.log(err);
		res.render("pages/login.html", {message: 'Some Error Ocurred.'});
	});
};

// route to the Home page
router.get('/', function(req, res) {
	var sess = req.session;
	if (sess && sess.userName) {
		dbops.markAttendance(sess.userId).then((date) => {
			res.render("pages/landing.html", {name: sess.userName, type: sess.userTypeId, code: sess.userCode, node: sess.userNode, metadata: constants.getMetaData()});
		}).catch((err) => {
			console.log("cant mark attendance", sess.userId, err);
			res.render("pages/landing.html", {name: sess.userName, type: sess.userTypeId, code: sess.userCode, node: sess.userNode, metadata: constants.getMetaData()});
		});
		
	} else {
		res.render("pages/login.html");
	}
});

// Login using link
router.get('/access', function(req, res) {
	let userCode = req.query.usercode;
	loginHandler (userCode, req, res);
	
});

// Login using login form
router.post('/login', function(req, res) {
	let userCode = req.body.usercode;
	loginHandler (userCode, req, res);
});

// Logout
router.get('/logout', function(req, res) {
	req.session.destroy((err) => {
        if (err) {
        	console.log('Logout Error:');
        	console.log(err);
        }
        res.render('pages/login.html', {message: 'Logged Out.'});
    });
});

// Upload Assignment
router.post('/upload', function(req, res) {
	var sess = req.session;
	let assignmentStart = new Date(constants.getMetaData().astart);
	let assignmentEnd = new Date(constants.getMetaData().aend);
	let currentDate = new Date();
	let isAssignmentAllowed = ((currentDate <= assignmentEnd) && (currentDate >= assignmentStart));
	if (sess && sess.userName && isAssignmentAllowed) {
		var form = new formidable.IncomingForm();
		form.parse(req, function (err, fields, files) {
			let assgnNo = fields.serial;
			let fileName = files.filetoupload.name;
			let filePath = files.filetoupload.path;
			let modifiedFileName = assgnNo + '_' + sess.userCode + '_' + sess.userName + '_' + constants.aid + '_' + (new Date().getTime()) + '_' + fileName;
			dbops.addAssignment(sess.userId, filePath, fileName, modifiedFileName, sess.userNode).then((data) => {
				res.render('pages/landing.html', {name: sess.userName, type: sess.userTypeId, code: sess.userCode, node: sess.userNode, metadata: constants.getMetaData(), upload: 'File ' + fileName + ' has been uploaded successfully.'});
			}).catch((err) => {
				console.log(err);
				res.render('pages/landing.html', {name: sess.userName, type: sess.userTypeId, code: sess.userCode, node: sess.userNode, metadata: constants.getMetaData(), upload: 'Upload Failed.'});
			});
		});
	} else if (sess && sess.userName) {
		res.render('pages/landing.html', {name: sess.userName, type: sess.userTypeId, code: sess.userCode, node: sess.userNode, metadata: constants.getMetaData(), upload: 'Upload Failed. Assignment can be uploaded in stipulated time window only.'});
	} else {
		res.render('pages/login.html', {message: 'Not Logged In.'});
	}
});

// Route to the admin page
router.get('/admin', function(req,res) {
	var sess = req.session;
	if (sess && sess.userName && sess.userTypeId && sess.userTypeId === 1) {
		res.render('pages/admin.html', {name: sess.userName, code: sess.userCode, node: sess.userNode, metadata: constants.getMetaData()});
	} else {
		res.render('pages/login.html', {message: 'Not Logged In or You are Unauthorized.'});
	}
});

// Update metadata: video
router.post('/update/video', function(req, res) {
	var sess = req.session;
	if (sess && sess.userName && sess.userTypeId && sess.userTypeId === 1) {
		let cls = req.body.class;
		let link = req.body.vlink;
		let desc = req.body.vdesc;
		let startTime = req.body.startHour + ':' + req.body.startMin;
		let endTime = req.body.endHour + ':' + req.body.endMin;
		let classDate = req.body.cdate;
		let nextClass = req.body.next;
		dbops.updateVideoMetaData(cls, link, desc, startTime, endTime, classDate, nextClass, sess.userCode).then((status) => {
			console.log('Updated Video Link by ' + sess.userName);
			console.log(constants.getMetaData());
			res.render('pages/admin.html', {name: sess.userName, code: sess.userCode, node: sess.userNode, metadata: constants.getMetaData(), vmsg: 'Update Successful'});
		}).catch((err) => {
			console.log(err);
			res.render('pages/admin.html', {name: sess.userName, code: sess.userCode, node: sess.userNode, metadata: constants.getMetaData(), vmsg: 'Update failed'});
		});
	} else {
		res.render('pages/login.html', {message: 'Not Logged In or You are Unauthorized.'});	
	}
});

// Update metadata: schedule
router.post('/update/schd', function(req, res) {
	var sess = req.session;
	if (sess && sess.userName && sess.userTypeId && sess.userTypeId === 1) {
		let markup = req.body.schd;
		dbops.updateScheduleMetaData(markup, sess.userCode).then((status) => {
			console.log('Updated Schedule by ' + sess.userName);
			console.log(constants.getMetaData());
			res.render('pages/admin.html', {name: sess.userName, code: sess.userCode, node: sess.userNode, metadata: constants.getMetaData(), smsg: 'Update Successful'});
		}).catch((err) => {
			console.log(err);
			res.render('pages/admin.html', {name: sess.userName, code: sess.userCode, node: sess.userNode, metadata: constants.getMetaData(), smsg: 'Update failed'});
		});
	} else {
		res.render('pages/login.html', {message: 'Not Logged In or You are Unauthorized.'});	
	}
});

// Update metadata: assignment
router.post('/update/assgn', function(req, res) {
	var sess = req.session;
	if (sess && sess.userName && sess.userTypeId && sess.userTypeId === 1) {
		let count = req.body.acount;
		let text = req.body.atext;
		let start = req.body.starttime;
		let end = req.body.endtime;
		if ((new Date(end) - new Date(start)) < 1000) {
			res.render('pages/admin.html', {name: sess.userName, code: sess.userCode, node: sess.userNode, metadata: constants.getMetaData(), amsg: 'Update failed; end time should be later than start time.'});
		} else {
			dbops.updateAssignmentMetaData(count, text, start, end, sess.userCode).then((status) => {
				console.log('Updated Assignment by ' + sess.userName);
				console.log(constants.getMetaData());
				res.render('pages/admin.html', {name: sess.userName, code: sess.userCode, node: sess.userNode, metadata: constants.getMetaData(), amsg: 'Update Successful'});
			}).catch((err) => {
				console.log(err);
				res.render('pages/admin.html', {name: sess.userName, code: sess.userCode, node: sess.userNode, metadata: constants.getMetaData(), amsg: 'Update failed'});
			});
		}
	} else {
		res.render('pages/login.html', {message: 'Not Logged In or You are Unauthorized.'});	
	}
});

router.get('/archives', function(req, res) {
	// res.render('pages/login.html', {message: 'Not Logged In or You are Unauthorized.'});
	/* As per 23.08.2020: archive page is removed; again activated on 11.10.2020 */
	/* As per 04.12.2020 archive removed again; again activated on 05.02.2021 */

	var sess = req.session;
	if (sess && sess.userName) {
		res.render('pages/archive.html', {name: sess.userName, code: sess.userCode, node: sess.userNode, data: constants.getMetaData().archv, mon: constants.getMonths()});
	} else {
		res.render('pages/login.html', {message: 'Not Logged In or You are Unauthorized.'});
	}
	
});

// Nodal personl screen
router.get('/nodal', function(req, res) {
	var sess = req.session;
	if (sess && sess.userName && sess.userTypeId && sess.userTypeId !== 3) {
		dbops.getNodes(sess.userTypeId, sess.userNode).then((data) => {
			res.render('pages/nodal.html', {name: sess.userName, code: sess.userCode, node: sess.userNode});
		}).catch((err) => {
			console.log(err);
			res.render('pages/nodal.html', {name: sess.userName, code: sess.userCode, node: sess.userNode, message: 'Some Error Occurred'});
		});
	} else {
		res.render('pages/login.html', {message: 'Not Logged In or You are Unauthorized.'});
	}
});

router.get('/nodes', function(req, res) {
	var sess = req.session;
	if (sess && sess.userName && sess.userTypeId && sess.userTypeId !== 3) {
		dbops.getNodes(sess.userTypeId, sess.userNodeId).then((data) => {
			let nodeValues = []; 
			data.forEach((node) => {
				nodeValues.push(node.col_code + ': ' + node.node_name);
			});
			 res.send(JSON.stringify(nodeValues));
		}).catch((err) => {
			console.log(err);
			res.send([]);
		});
	} else {
		res.send([]);
	}
});

router.post('/nodal', function(req, res) {
	var sess = req.session;
	if (sess && sess.userName && sess.userTypeId && sess.userTypeId !== 3) {
		let date = req.body.date;
		let type = req.body.type;
		let node = req.body.node;
		if (type && type === 'list' && node) {
			dbops.getRegisteredUsers(sess.userTypeId, sess.userColCode, node).then((data) => {
				res.render('pages/nodal.html', {name: sess.userName, code: sess.userCode, node: sess.userNode, users: data, collg: node, date: date, type: type});	
			}).catch((err) => {
				console.log(err);
				res.render('pages/nodal.html', {name: sess.userName, code: sess.userCode, node: sess.userNode, message: "Some Error Occurred", collg: node, date: date, type: type});
			});
		} else if (type && type === 'attn' && node && date) {
			dbops.getAttandancesNode(sess.userTypeId, sess.userColCode, node, date).then((data) => {
				res.render('pages/nodal.html', {name: sess.userName, code: sess.userCode, node: sess.userNode, attn: data, collg: node, date: date, type: type});	
			}).catch((err) => {
				console.log(err);
				res.render('pages/nodal.html', {name: sess.userName, code: sess.userCode, node: sess.userNode, message: "Some Error Occurred", collg: node, date: date, type: type});
			});
		} else {
			res.render('pages/nodal.html', {name: sess.userName, code: sess.userCode, node: sess.userNode, message: "Please provide all input values", collg: node, date: date, type: type});
		}
	} else {
		res.render('pages/login.html', {message: 'Not Logged In or You are Unauthorized.'});
	}
});

router.get('/rep1', (req, res) => {
	var sess = req.session;
	if (sess && sess.userName && sess.userTypeId && sess.userTypeId === 1) {
		dbops.getUploadCountReport().then((data) => {
			res.render('pages/reports/report-1.html', {name: sess.userName, code: sess.userCode, node: sess.userNode, data: data});	
		}).catch((err) => {
			console.log(err);
			res.render('pages/reports/report-1.html', {name: sess.userName, code: sess.userCode, node: sess.userNode, data: [], message: "Some Error Occurred"});	
		});
	} else {
		res.render('pages/login.html', {message: 'Not Logged In or You are Unauthorized.'});
	}
});

router.get('/rep2', (req, res) => {
	var sess = req.session;
	if (sess && sess.userName && sess.userTypeId && sess.userTypeId === 1 && parseInt(sess.userId) <= 2) {
		dbops.importUsers().then((data) => {
			res.render('pages/reports/report-2.html', {name: sess.userName, code: sess.userCode, node: sess.userNode, data: data});	
		}).catch((err) => {
			console.log(err);
			res.render('pages/reports/report-2.html', {name: sess.userName, code: sess.userCode, node: sess.userNode, data: [], message: "Some Error Occurred"});	
		});
	} else {
		res.render('pages/login.html', {message: 'Not Logged In or You are Unauthorized.'});
	}
});

router.get('/rep3', (req, res) => {
	var sess = req.session;
	if (sess && sess.userName && sess.userTypeId && sess.userTypeId === 1 && parseInt(sess.userId) <= 2) {
		dbops.nodalSMS().then((data) => {
			res.render('pages/reports/report-3.html', {name: sess.userName, code: sess.userCode, node: sess.userNode, data: data});	
		}).catch((err) => {
			console.log(err);
			res.render('pages/reports/report-3.html', {name: sess.userName, code: sess.userCode, node: sess.userNode, data: [], message: "Some Error Occurred"});	
		});
	} else {
		res.render('pages/login.html', {message: 'Not Logged In or You are Unauthorized.'});
	}
});

router.get('/rep4', (req, res) => {
	var sess = req.session;
	if (sess && sess.userName && sess.userTypeId && sess.userTypeId === 1) {
		dbops.paymentReport().then((data) => {
			res.render('pages/reports/report-4.html', {name: sess.userName, code: sess.userCode, node: sess.userNode, data: data});	
		}).catch((err) => {
			console.log(err);
			res.render('pages/reports/report-4.html', {name: sess.userName, code: sess.userCode, node: sess.userNode, data: [], message: "Some Error Occurred"});	
		});
	} else {
		res.render('pages/login.html', {message: 'Not Logged In or You are Unauthorized.'});
	}
});
pkill -f "node eduClassesServer.js"
npm install

data=`date +%d%m%yT%H%M%S`
mv logs/stdout.txt logs/stdout.txt.${data}
mv logs/stderr.txt logs/stderr.txt.${data}

nohup node eduClassesServer.js > logs/stdout.txt 2> logs/stderr.txt &